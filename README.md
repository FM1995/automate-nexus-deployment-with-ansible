# Automate Nexus Deployment using Ansible

#### Project Outline

In this project we will install and start nexus on a remote server
So instead of SSH’ing into the server and executing, download the Nexus binary and unpack, then run the Nexus application using Nexus user. We can do all of that using Ansible. Again advantages of this is that we can execute the script repeatedly on a number of scripts.

#### Lets get started

Lets create a new droplet

![Image1](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image1.png)

Starting off with the host of the new droplet

![Image2](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image2.png)

Now lets create our first play

Let’s do java and net-tools

In this case we will specify Java 8

![Image3](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image3.png)

Now for the second play

We will download and unpack the Nexus Installer

![Image4](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image4.png)

Will be using the below link to install the nexus package

https://help.sonatype.com/repomanager3/product-information/download

![Image5](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image5.png)

and then using the below to implement get modules to install the above

https://docs.ansible.com/ansible/latest/collections/ansible/builtin/get_url_module.html

Below is an example 

![Image6](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image6.png)

Can then run it again

![Image7](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image7.png)

Now let’s try and run it

```
ansible-playbook -I hosts deploy-nexus.yaml
```

And can see it ran successfully

![Image8](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image8.png)

Now going into the server can see the below that the file got downloaded

![Image9](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image9.png)

Can see the zip file and java version aswell from the installation from the playbook

![Image10](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image10.png)

Can then untar it

![Image11](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image11.png)

Can also set it to it’s location by setting the remote src

![Image12](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image12.png)

Can then re-run the command and obtain the below

![Image13](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image13.png)

Can see it got un packaged

![Image14](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image14.png)

One issue is that zip file versions being put into the playbook is not optimal

So let’s start by removing the already existing one

![Image15](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image15.png)

And then we will save the result of the download and save it to a message

![Image16](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image16.png)

Upon checking the results can see the a key called ‘dest’ is where the location is saved

![Image17](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image17.png)

Can then configure the below, which will grab the ‘dest’ to which will come from the download result

![Image18](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image18.png)

Can then run the playbook again

![Image19](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image19.png)

Can see it the below

![Image20](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image20.png)

Now let’s re-name and move the file, but first we can find the file/directory using the below

![Image21](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image21.png)

Re-running the playbook we can see the full path of the folder

![Image22](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image22.png)

Now that we can get the results, we can find navigate to the files attribute, where the files is a list and then specifying the .path attribute, can then move it to the nexus directory

Below is what has been implemented

![Image23](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image23.png)

Can then re-run the command and see the re-name was successful

![Image24](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image24.png)

Can see the file re-named called nexus without the version 3.64.0-01

![Image25](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image25.png)

Now we have to implement logic and see if the nexus folder already exists using ‘stat’

![Image26](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image26.png)

Can then run it, and it will display the below

![Image27](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image27.png)

Going down further can see the key exists

![Image28](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image28.png)

Now we can modify our playbook to include a script

Can then do it in the debug message

![Image29](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image29.png)

Can then configure the below

![Image30](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image30.png)

Upon running can see the below is skipping the rename part as it already exists

![Image31](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image31.png)

Next we need to modify the user so that it has the ability to run nexus commands

![Image32](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image32.png)

Let’s start off by creating a new play

![Image33](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image33.png)

However creating a new user and group is a little different in Ansible, so first we need to Create nexus group and then assign the nexus user to the group

![Image34](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image34.png)

Now lets do the same for sonatype folder

![Image35](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image35.png)

Resolving the below

![Image36](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image36.png)

Now we are able to execute and can see the results

![Image37](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image37.png)

Can then see the below

![Image38](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image38.png)

Next we need to start nexus as nexus user, can implement it like the below using ‘blockinfile’

![Image39](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image39.png)

Now lets execute this

![Image40](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image40.png)

Let’s now check

![Image41](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image41.png)

![Image42](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image42.png)

Now lets do some finer changes, where we can include linefile module, where it ensure a particular line is in a file, or replace an existing line using regex.

Let’s remove the three lines

![Image43](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image43.png)

Can then do it like the below

![Image44](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image44.png)

Can then introduce it like the below

![Image45](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image45.png)

Can then execute again

![Image46](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image46.png)

Can then put in place the condition to see if the folder exists or not

![Image47](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image47.png)

Can then re-run the and see that the folder is skipping untar stages

![Image48](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image48.png)

Next step we will switch to the user and start nexus

![Image49](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image49.png)

Can then re-run and see the ansible output

![Image50](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image50.png)

If we back to the server

![Image51](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image51.png)

And can see insufficient memory

![Image52](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image52.png)

Will destroy and create a new droplet

![Image53](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image53.png)

Newly created droplet

![Image54](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image54.png)

Making the change in hosts file

![Image55](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image55.png)

Can then add host name for reference

![Image56](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image56.png)

Can then re-run the below

![Image57](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image57.png)

Everything got executed

![Image58](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image58.png)

Now ssh’ing into the server

```
ps aux | grep nexus
```

![Image59](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image59.png)

Now instead of going into the droplet/server, and running the grep command, can utilise the below play

![Image60](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image60.png)

First let’s kill the process

![Image61](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image61.png)

Can then execute the playbook

![Image62](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image62.png)

Can also see the below

![Image63](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image63.png)

Using the below documentation, we can get it to pause, so that in our playbook the ports will display

https://docs.ansible.com/ansible/latest/collections/ansible/builtin/pause_module.html

Just like the below

![Image64](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image64.png)

Can put this in the below

![Image65](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image65.png)

Upon running can see the java port is open in the terminal logs

![Image66](https://gitlab.com/FM1995/automate-nexus-deployment-with-ansible/-/raw/main/Images/Image66.png)







